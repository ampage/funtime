#! /usr/bin/python

'''
Common Lisp style sequence manipulation functions for Python.

Not all functions are implemented yet, usually only the most basic ones like
member, find etc. and not member-if, find-if etc. Except for remove-if-not,
which I have done instead of remove-if, since it's more useful.

Might it be worth having two versions of each function: one that returns an
iterator/map object etc, and one that returns an actual list? Could benefit
performance a lot. Yes: settle on naming convention.

Do I really need the :start and :end kwargs? Python does have covenient
splicing.

Is it possible to factor out repetition in member_, remove_if_not,
remove_duplicates?
'''

# TODO
'''
-Clean up.
-Carry on.
-Make decisions.
-Write tests!!!
'''

import operator as op
import functools as fun
import itertools as itr
import importlib as imp

# The basics.

def identity(thing):
    return thing

def complement(fn):
    def comp(*args):
        return not fn(*args)
    return comp

def constantly(value):
    def const(*args):
        return value
    return const

def traversal(seq, from_end):
    '''Yield the indices required to traverse SEQ from either end.'''
    if from_end:
        for i in range((len(seq) - 1), -1, -1):
            yield i
    else:
        for i in range(len(seq)):
            yield i

def non_negative_integer_p(x):
    return (x >= 0 and isinstance(x, int))

def evenp(x):
    return x % 2 == 0

def oddp(x):
    return not evenp(x)

def reverse(seq):
    seq = seq.copy()
    seq.reverse()
    return seq

def nreverse(seq):
    seq.reverse()
    return seq

# Reducing - for now, use Haskell conventions, since implementing reduce with
# all those keyword arguments is non-trivial.

def foldl(fn, k, xs):
    return fun.reduce(fn, xs, k)

def flip(fn):
    def new_fn(x, y):
        return fn(y, x)
    return new_fn

def foldr(fn, acc, xs):
    return fun.reduce(flip(fn), reversed(xs), acc)

# Variadic versions of binary Python operators.

def add(*args):
    return sum(args)

def sub(n, *args):
    if args:
        return foldl(op.sub, n, args)
    else:
        return op.neg(n)

def mul(*args):
    return foldl(op.mul, 1, args)

def div(n, *args):
    if args:
        return foldl(op.truediv, n, args)
    else:
        return 1 / n

def equals(*args):
    '''Variadic version of Python's default equality predicate (which inspects non-atoms).'''
    assert args
    if len(args) == 1:
        return True
    else:
        return foldl(op.eq, args[0], args)

def listify(*args):
    return list(args)

# Finding stuff

def member_(item, seq, key=identity, test=op.eq, from_end=False, start=0, end=None):
    '''Helper function for member, position and find (only their return values differ).'''
    if end:
        assert end in range(len(seq) + 1)
    else:
        end = len(seq) + 1
    seq = seq[start:end]
    for i in traversal(seq, from_end):
        if test(key(seq[i]), item):
            return i, seq[i]

def member(item, seq, key=identity, test=op.eq):
    is_member = member_(item, seq, key, test)[0]
    if is_member:
        return seq[is_member:]

# The Pythonic equivalent is seq.index(item).
def position(item, seq, key=identity, test=op.eq, from_end=False, start=0, end=None):
    return member_(item, seq, key, test, from_end, start, end)[0]

def find(item, seq, key=identity, test=op.eq, from_end=False, start=0, end=None):
    return member_(item, seq, key, test, from_end, start, end)[1]

# Filtering and removing

# The Pythonic ways:
# seq.remove(item)
# del seq[i]        #destructive

#list(filter(lambda x: x % 2 == 0, [1,2,3,4])) # or:
#[x for x in [1,2,3,4] if x % 2 == 0]

def remove_if_not(pred, seq, key=identity, from_end=False, start=0, end=None, count=None):
    '''Count behaviour implemented inefficiently (KISS), since I rarely use that.'''
    if count:
        assert non_negative_integer_p(count)
    if end:
        assert end in range(len(seq) + 1)
    else:
        end = len(seq) + 1
    seq = seq[start:end]
    result = []
    for i in traversal(seq, from_end):
        if pred(key(seq[i])):
            result.append(seq[i])
    if count:
        result = result[:count]
    if from_end:
        return nreverse(result)
    else:
        return result

'''
Debug this.

def remove(item, seq, key=identity, test=op.eq,
           from_end=False, start=0, end=None, count=1):
    return remove_if_not(fun.partial(complement(test), item),
                         seq, key, from_end, start, end, count)
'''

# The Pythonic ways:

#list(set([1,2,2,3,4,4,5,5,6])) # naive, destroys order
#[x for n,x in enumerate(seq) if x not in seq[:n]]

def remove_duplicates(seq, key=identity, test=op.eq, from_end=None, start=0, end=None):
    if end:
        assert end in range(len(seq) + 1)
    else:
        end = len(seq) + 1
    seq = seq[start:end]
    result = []
    for i in traversal(seq, from_end):
        if not member_(key(seq[i]), result, key, test, from_end):
            result.append(seq[i])
    if from_end:
        return nreverse(result)
    else:
        return result

# Mapping

#list(map((lambda x: x + 1), [1,2,3,4])) #have to force list creation, so prefer list comps.
#[x + 1 for x in [1,2,3,4]]

def mapcar(fn, seq, *args):
    return list(map(fn, seq, *args))

# Note that in CL, fn() passed to mapcan must return a list. How to enforce that?

#def mapcan(fn, seq, *args):
#    return list(itr.chain(map(fn, seq, *args)))

# Sorting

'''
In Python 3, the sorting functions no longer allow a comparator argument. Instead,
the sequence you're supporting needs to have a custom __cmp__() method (I think),
which is super-annoying, since then you have to design your own sequence class
instead of just using lists. They do however take a reverse argument, which is like
(complement my-sorting-predicate). Destructively sort using seq.sort(), safely using
sorted(seq).
'''

# Counting

# seq.count(item)

# Tests
